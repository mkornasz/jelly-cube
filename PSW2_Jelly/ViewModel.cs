﻿using System;
using System.Collections.Generic;
using SharpGL;
using SharpGL.SceneGraph;
using GLfloat = System.Single;
using Microsoft.Practices.Prism.Commands;
using System.Windows.Input;
using System.Windows;
using System.Windows.Threading;

namespace PSW2_Jelly
{
    class ViewModel : ViewModelBase
    {
        private Random _randomizer;
        private DispatcherTimer _simulationTimer;
        private OpenGL gl;
        private bool _isMouseDown, _isIdeal = false, _isInitialized = false;
        private float _rotateHor, _rotateVer, _rotateZ, _scale;
        private double _oldMouseX, _oldMouseY, _mouseX, _mouseY, _traX, _traY, _lengthSteringCube, _maxStartVel;
        private GLfloat[] vertices2, normals2, colors2;
        private uint[] indices;
        private VertexEx[,,] controlPoints;
        private Vertex boundingCubeFront, boundingCubeBack;
        private Vertex[] insideCubeVertices;
        private int[] insideCubeIndices;
        private bool _showControlPoints, _showSteringCube, _showBoundingCubeLines;
        private double _k, _c1, _c2, _m, _u;
        private Simulation[,,] pointSimulations;
        #region Propierties

        public string LengthStering
        {
            get { return _lengthSteringCube.ToString(); }
            set
            {
                if (_lengthSteringCube.ToString() != value)
                {
                    if (value.Length != 0)
                        _lengthSteringCube = double.Parse(value);
                    else
                        _lengthSteringCube = 0;
                    OnPropertyChanged("LengthStering");
                }
            }
        }
        public string StartVel
        {
            get { return _maxStartVel.ToString(); }
            set
            {
                if (_maxStartVel.ToString() != value)
                {
                    if (value.Length != 0)
                        _maxStartVel = double.Parse(value);
                    else
                        _maxStartVel = 0;
                    OnPropertyChanged("StartVel");
                }
            }
        }
        public string Mass
        {
            get { return _m.ToString(); }
            set
            {
                if (_m.ToString() != value)
                {
                    if (value.Length != 0)
                        _m = double.Parse(value);
                    else
                        _m = 0;
                    OnPropertyChanged("Mass");
                }
            }
        }
        public string FactorU
        {
            get { return _u.ToString(); }
            set
            {
                if (_u.ToString() != value)
                {
                    if (value.Length != 0)
                        _u = double.Parse(value);
                    else
                        _u = 0;
                    OnPropertyChanged("FactorU");
                }
            }
        }
        public string FactorK
        {
            get { return _k.ToString(); }
            set
            {
                if (_k.ToString() != value)
                {
                    if (value.Length != 0)
                        _k = double.Parse(value);
                    else
                        _k = 0;
                    OnPropertyChanged("FactorK");
                }
            }
        }
        public string FactorC1
        {
            get { return _c1.ToString(); }
            set
            {
                if (_c1.ToString() != value)
                {
                    if (value.Length != 0)
                        _c1 = double.Parse(value);
                    else
                        _c1 = 0;
                    OnPropertyChanged("FactorC1");
                }
            }
        }
        public string FactorC2
        {
            get { return _c2.ToString(); }
            set
            {
                if (_c2.ToString() != value)
                {
                    if (value.Length != 0)
                        _c2 = double.Parse(value);
                    else
                        _c2 = 0;
                    OnPropertyChanged("FactorC2");
                }
            }
        }
        public bool IdealCollisions
        {
            get
            {
                return _isIdeal;
            }
            set
            {
                if (_isIdeal != value)
                {
                    _isIdeal = value;
                    OnPropertyChanged("IdealCollisions");
                }
            }
        }
        public bool ShowSteringCube
        {
            get
            {
                return _showSteringCube;
            }
            set
            {
                if (_showSteringCube != value)
                {
                    _showSteringCube = value;
                    OnPropertyChanged("ShowSteringCube");
                }
            }
        }
        public bool ShowControlPoints
        {
            get
            {
                return _showControlPoints;
            }
            set
            {
                if (_showControlPoints != value)
                {
                    _showControlPoints = value;
                    OnPropertyChanged("ShowControlPoints");
                }
            }
        }
        public bool ShowBoundingLines
        {
            get
            {
                return _showBoundingCubeLines;
            }
            set
            {
                if (_showBoundingCubeLines != value)
                {
                    _showBoundingCubeLines = value;
                    OnPropertyChanged("ShowBoundingLines");
                }
            }
        }
        #endregion
        #region Commands
        private ICommand _openGLDrawExecutedCommand, _mouseDownExecutedCommand, _mouseUpExecutedCommand, _restartSymExecutedCommand, _mouseMoveExecutedCommand, _mouseWheelExecutedCommand;
        public ICommand RestartSimulation
        {
            get { return _restartSymExecutedCommand ?? (_restartSymExecutedCommand = new DelegateCommand(TimerRestart)); }
        }
        public ICommand DrawCommand
        {
            get { return _openGLDrawExecutedCommand ?? (_openGLDrawExecutedCommand = new DelegateCommand(OpenGLDrawExecuted)); }
        }
        public ICommand MouseDownCommand
        {
            get { return _mouseDownExecutedCommand ?? (_mouseDownExecutedCommand = new DelegateCommand(MouseDownExecuted)); }
        }
        public ICommand MouseUpCommand
        {
            get { return _mouseUpExecutedCommand ?? (_mouseUpExecutedCommand = new DelegateCommand(MouseUpExecuted)); }
        }
        public ICommand MouseMoveCommand
        {
            get { return _mouseMoveExecutedCommand ?? (_mouseMoveExecutedCommand = new DelegateCommand(MouseMoveExecuted)); }
        }
        public ICommand MouseWheelCommand
        {
            get { return _mouseWheelExecutedCommand ?? (_mouseWheelExecutedCommand = new DelegateCommand(MouseWheelExecuted)); }
        }
        private void MouseWheelExecuted()
        {
            if (!Keyboard.IsKeyDown(Key.LeftShift))
                return;

            int a = Mouse.MouseWheelDeltaForOneLine / 5;
            float numSteps = Mouse.RightButton == MouseButtonState.Pressed ? -a / 15 : a / 15;
            if (_scale + numSteps / 50 > 0)
                _scale += numSteps / 50;
        }
        private void MouseDownExecuted()
        {
            Point mouseXY = Mouse.GetPosition((Application.Current.MainWindow as MainWindow).OpenGlContr);
            _mouseX = mouseXY.X;
            _mouseY = mouseXY.Y;
            _isMouseDown = true;
        }
        private void MouseUpExecuted()
        {
            _isMouseDown = false;
        }
        private void MouseMoveExecuted()
        {
            if (!_isMouseDown)
                return;

            if (Keyboard.IsKeyDown(Key.LeftShift))
            {
                _oldMouseX = _mouseX;
                _oldMouseY = _mouseY;

                Point mouseXY = Mouse.GetPosition((Application.Current.MainWindow as MainWindow).OpenGlContr);
                _mouseX = mouseXY.X;
                _mouseY = mouseXY.Y;

                if (Mouse.RightButton == MouseButtonState.Pressed)
                {
                    _traX += (_mouseX - _oldMouseX) / 100;
                    _traY += -(_mouseY - _oldMouseY) / 100;
                }
                else
                {
                    if ((_mouseX - _oldMouseX) > 0)
                        _rotateHor += 3.0f;
                    else if ((_mouseX - _oldMouseX) < 0)
                        _rotateHor -= 3.0f;

                    if (Keyboard.IsKeyDown(Key.Z))
                    {
                        if ((_mouseY - _oldMouseY) > 0)
                            _rotateZ += 3.0f;
                        else if ((_mouseY - _oldMouseY) < 0)
                            _rotateZ -= 3.0f;
                    }
                    else
                    {
                        if ((_mouseY - _oldMouseY) > 0)
                            _rotateVer += 3.0f;
                        else if ((_mouseY - _oldMouseY) < 0)
                            _rotateVer -= 3.0f;
                    }
                }
            }
            else
                MoveInsideCube();
        }

        #endregion

        public ViewModel()
        {
            gl = new OpenGL();
            _randomizer = new Random();
            InitializeVariables();
            _simulationTimer = new DispatcherTimer { Interval = new TimeSpan(0, 0, 0, 0, 1) };
            _simulationTimer.Tick += simulationTick;

        }


        private void OpenGLDrawExecuted()
        {
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            //   gl.PolygonMode(OpenGL.GL_FRONT_AND_BACK, OpenGL.GL_LINE);
            gl.LoadIdentity();
            gl.Translate(_traX, _traY, -3.0);
            gl.Rotate(_rotateVer, _rotateHor, _rotateZ);
            gl.Scale(_scale, _scale, _scale);


            if (!_isInitialized)
            {
                BuildBoundingCube();
                //  BuildJellyCube();
                BuildInsideCube();
                gl.Enable(OpenGL.GL_DEPTH_TEST);

                float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
                float[] light0pos = new float[] { 7.0f, 2.0f, 0.0f, 1.0f };
                // float[] light1pos = new float[] { 7.0f, -7.0f, 0.0f, 1.0f };
                float[] light0ambient = new float[] { 0.0f, 0.0f, 0.9f, 1.0f };
                float[] light1ambient = new float[] { 1.0f, 0.0f, 0.0f, 1.0f };
                float[] light0diffuse = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
                float[] light1diffuse = new float[] { 1.0f, 1.0f, 1.0f, 1.0f };
                float[] light0specular = new float[] { 0.1f, 0.1f, 0.1f, 1.0f };

                float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };

                gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
                gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
                gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
                gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
                gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);

                gl.Light(OpenGL.GL_LIGHT1, OpenGL.GL_POSITION, light0pos);
                gl.Light(OpenGL.GL_LIGHT1, OpenGL.GL_AMBIENT, light1ambient);
                gl.Light(OpenGL.GL_LIGHT1, OpenGL.GL_DIFFUSE, light1diffuse);
                gl.Light(OpenGL.GL_LIGHT1, OpenGL.GL_SPECULAR, light0specular);

                gl.Enable(OpenGL.GL_LIGHTING);
                gl.Enable(OpenGL.GL_LIGHT0);
                gl.ShadeModel(OpenGL.GL_SMOOTH);
                _isInitialized = true;


                _simulationTimer.Start();

            }

            DisplayInside();
            DisplayBoundingCube(0);
        }
        private void BuildBoundingCube()
        {
            boundingCubeBack = new Vertex(5, 3, 5);
            boundingCubeFront = new Vertex(0, 0, 0);


            vertices2 = new GLfloat[] { 5, 3, 5,  0, 3, 5,  0,0, 5,   5,0, 5,
                                        5, 3, 5,   5,0, 5,   5,0,0,   5,3,0,
                                        5, 3, 5,   5, 3,0,  0, 3,0,  0, 3, 5,
                                        0, 3, 5,  0, 3,0,  0,0,0,  0,0, 5,
                                        0,0,0,   5,0,0,   5,0, 5,  0,0, 5,
                                            5,0,0,  0,0,0,  0, 3,0,   5, 3,0 };

            normals2 = new GLfloat[] { 0, 0, 1,   0, 0, 1,   0, 0, 1,   0, 0, 1,
                        1, 0, 0,   1, 0, 0,   1, 0, 0,   1, 0, 0,
                        0, 1, 0,   0, 1, 0,   0, 1, 0,   0, 1, 0,
                       0, 0, 0,  0, 0, 0,  0, 0, 0,  0, 0, 0,
                        0,0, 0,   0,0, 0,   0,0, 0,   0,0, 0,
                        0, 0,0,   0, 0,0,   0, 0,0,   0, 0,0 };


            indices = new uint[]{ 0, 1, 2,   2, 3, 0,
                       4, 5, 6,   6, 7, 4,
                       8, 9,10,  10,11, 8,
                      12,13,14,  14,15,12,
                      16,17,18,  18,19,16,
                      20,21,22,  22,23,20 };
        }

        private void BuildInsideCube()
        {
            float delta = 1 / 3.0f;
            insideCubeVertices = new Vertex[8];
            controlPoints = new VertexEx[4, 4, 4];
            pointSimulations = new Simulation[4, 4, 4];
            List<Vertex> tmp;
            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++)
                    for (int k = 0; k < 4; k++)
                    {
                        tmp = new List<Vertex>();
                        if (i > 0)
                            tmp.Add(new Vertex(i - 1, j, k));
                        if (i < 3)
                            tmp.Add(new Vertex(i + 1, j, k));
                        if (j > 0)
                            tmp.Add(new Vertex(i, j - 1, k));
                        if (j < 3)
                            tmp.Add(new Vertex(i, j + 1, k));
                        if (k > 0)
                            tmp.Add(new Vertex(i, j, k - 1));
                        if (k < 3)
                            tmp.Add(new Vertex(i, j, k + 1));

                        if (i > 0 && j > 0)
                            tmp.Add(new Vertex(i - 1, j - 1, k));
                        if (i < 3 && j > 0)
                            tmp.Add(new Vertex(i + 1, j - 1, k));
                        if (i > 0 && j < 3)
                            tmp.Add(new Vertex(i - 1, j + 1, k));
                        if (i < 3 && j < 3)
                            tmp.Add(new Vertex(i + 1, j + 1, k));

                        if (i > 0 && k > 0)
                            tmp.Add(new Vertex(i - 1, j, k - 1));
                        if (i < 3 && k > 0)
                            tmp.Add(new Vertex(i + 1, j, k - 1));
                        if (i > 0 && k < 3)
                            tmp.Add(new Vertex(i - 1, j, k + 1));
                        if (i < 3 && k < 3)
                            tmp.Add(new Vertex(i + 1, j, k + 1));

                        if (j > 0 && k > 0)
                            tmp.Add(new Vertex(i, j - 1, k - 1));
                        if (j < 3 && k > 0)
                            tmp.Add(new Vertex(i, j + 1, k - 1));
                        if (j > 0 && k < 3)
                            tmp.Add(new Vertex(i, j - 1, k + 1));
                        if (j < 3 && k < 3)
                            tmp.Add(new Vertex(i, j + 1, k + 1));

                        controlPoints[i, j, k] = new VertexEx(new Vertex(i * delta, j * delta, k * delta), tmp, 0.5 + _randomizer.NextDouble() * _maxStartVel, 0.5 + _randomizer.NextDouble() * _maxStartVel);

                    }


            var a = controlPoints[0, 0, 0];
            insideCubeVertices[0] = new Vertex(controlPoints[0, 0, 0].vert.X, controlPoints[0, 0, 0].vert.Y, controlPoints[0, 0, 0].vert.Z);
            insideCubeVertices[1] = new Vertex(controlPoints[3, 0, 0].vert.X, controlPoints[3, 0, 0].vert.Y, controlPoints[3, 0, 0].vert.Z);
            insideCubeVertices[2] = new Vertex(controlPoints[0, 3, 0].vert.X, controlPoints[0, 3, 0].vert.Y, controlPoints[0, 3, 0].vert.Z);
            insideCubeVertices[3] = new Vertex(controlPoints[0, 0, 3].vert.X, controlPoints[0, 0, 3].vert.Y, controlPoints[0, 0, 3].vert.Z);
            insideCubeVertices[4] = new Vertex(controlPoints[3, 3, 0].vert.X, controlPoints[3, 3, 0].vert.Y, controlPoints[3, 3, 0].vert.Z);
            insideCubeVertices[5] = new Vertex(controlPoints[3, 0, 3].vert.X, controlPoints[3, 0, 3].vert.Y, controlPoints[3, 0, 3].vert.Z);
            insideCubeVertices[6] = new Vertex(controlPoints[0, 3, 3].vert.X, controlPoints[0, 3, 3].vert.Y, controlPoints[0, 3, 3].vert.Z);
            insideCubeVertices[7] = new Vertex(controlPoints[3, 3, 3].vert.X, controlPoints[3, 3, 3].vert.Y, controlPoints[3, 3, 3].vert.Z);

            controlPoints[0, 0, 0].neighboursCoord.Add(new Vertex(0, -1, -1));
            controlPoints[3, 0, 0].neighboursCoord.Add(new Vertex(1, -1, -1));
            controlPoints[0, 3, 0].neighboursCoord.Add(new Vertex(2, -1, -1));
            controlPoints[0, 0, 3].neighboursCoord.Add(new Vertex(3, -1, -1));
            controlPoints[3, 3, 0].neighboursCoord.Add(new Vertex(4, -1, -1));
            controlPoints[3, 0, 3].neighboursCoord.Add(new Vertex(5, -1, -1));
            controlPoints[0, 3, 3].neighboursCoord.Add(new Vertex(6, -1, -1));
            controlPoints[3, 3, 3].neighboursCoord.Add(new Vertex(7, -1, -1));

            insideCubeIndices = new int[24] { 0, 1, 0, 2, 0, 3, 4, 1, 4, 2, 4, 7, 5, 1, 5, 3, 5, 7, 6, 2, 6, 3, 6, 7 };


            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++)
                    for (int k = 0; k < 4; k++)
                    {
                        controlPoints[i, j, k].neighboursCoordDistances = new List<Vertex>(CalculateStartDistancesVertices(i, j, k));
                        controlPoints[i, j, k].neighboursDistances = new List<double>(CalculateStartDistances(i, j, k));

                        //tmpVel = new List<double>();
                        //tmpPos = new List<double>();
                        //for (int a = 0; a < controlPoints[i, j, k].neighboursCoord.Count; a++)
                        //{
                        //    if (controlPoints[i, j, k].neighboursCoord[a].Z == -1)
                        //    {
                        //        tmpVel.Add(0.5 + _randomizer.NextDouble() * _maxStartVel);
                        //        tmpPos.Add(0.5 + _randomizer.NextDouble() * _maxStartVel);
                        //    }
                        //    else
                        //    {
                        //        index = controlPoints[i, j, k].neighboursCoord[a];
                        //        tmpVel.Add(controlPoints[(int)index.X, (int)index.Y, (int)index.Z].startVelocity);
                        //        tmpPos.Add(controlPoints[(int)index.X, (int)index.Y, (int)index.Z].startPosition);
                        //    }
                        //}
                        pointSimulations[i, j, k] = new Simulation(_m, _c1, _c2, _k, controlPoints[i, j, k].vert, controlPoints[i, j, k].neighboursDistances, controlPoints[i, j, k].neighboursCoordDistances, RandomizeStart(), RandomizeStart());//, controlPoints[i, j, k].startVelocity);
                    }
        }

        private Vertex RandomizeStart()
        {
            var vx = _randomizer.NextDouble() * _maxStartVel;
            var vy = _randomizer.NextDouble() * _maxStartVel;
            var vz = _randomizer.NextDouble() * _maxStartVel;

            return new Vertex((float)vx, (float)vy, (float)vz);
        }


        private void MoveInsideCube()
        {
            _oldMouseX = _mouseX;
            _oldMouseY = _mouseY;

            Point mouseXY = Mouse.GetPosition((Application.Current.MainWindow as MainWindow).OpenGlContr);
            _mouseX = mouseXY.X;
            _mouseY = mouseXY.Y;

            double deltaX = (_mouseX - _oldMouseX) / 100;
            double deltaY = -(_mouseY - _oldMouseY) / 100;
            Matrix transfMatrix, tmp;

            if (Mouse.RightButton == MouseButtonState.Pressed)
            {
                if (Keyboard.IsKeyDown(Key.Z))
                    transfMatrix = new Matrix(new double[4, 4] { { 1, 0, 0, deltaY }, { 0, 1, 0, 0 }, { 0, 0, 1, deltaX }, { 0, 0, 0, 1 } });
                else
                    transfMatrix = new Matrix(new double[4, 4] { { 1, 0, 0, 0 }, { 0, 1, 0, deltaY }, { 0, 0, 1, deltaX }, { 0, 0, 0, 1 } });
            }
            else
            {
                transfMatrix = Keyboard.IsKeyDown(Key.X) ?
                    new Matrix(new double[4, 4] { { 1, 0, 0, 0 }, { 0, Math.Cos(deltaY), -Math.Sin(deltaY), 0 }, { 0, Math.Sin(deltaY), Math.Cos(deltaY), 0 }, { 0, 0, 0, 1 } })
                    :
                    Keyboard.IsKeyDown(Key.Y) ?
                    new Matrix(new double[4, 4] { { Math.Cos(deltaY), 0, Math.Sin(deltaY), 0 }, { 0, 1, 0, 0 }, { -Math.Sin(deltaY), 0, Math.Cos(deltaY), 0 }, { 0, 0, 0, 1 } })
                    :
                    Keyboard.IsKeyDown(Key.Z) ?
                    new Matrix(new double[4, 4] { { Math.Cos(deltaY), -Math.Sin(deltaY), 0, 0 }, { Math.Sin(deltaY), Math.Cos(deltaY), 0, 0 }, { 0, 0, 1, 0 }, { 0, 0, 0, 1 } })
                    :
                    new Matrix(Matrix.Identity(4));
            }

            for (int i = 0; i < insideCubeVertices.Length; i++)
            {
                tmp = new Matrix(new double[4, 1] { { insideCubeVertices[i].X }, { insideCubeVertices[i].Y }, { insideCubeVertices[i].Z }, { 1 } });
                tmp = transfMatrix * tmp;
                for (int t = 0; t < 4; t++)
                    tmp[t, 0] /= tmp[3, 0];
                insideCubeVertices[i] = new Vertex((float)tmp[0, 0], (float)tmp[1, 0], (float)tmp[2, 0]);
            }
        }
        private void DisplayBezierSurfaces(int xyz, int side)
        {
            List<double> singlePoints = new List<double>();

            switch (xyz)
            {
                case 0:
                    for (int j = 0; j < 4; j++)
                        for (int k = 0; k < 4; k++)
                        {
                            singlePoints.Add(controlPoints[side, j, k].vert.X);
                            singlePoints.Add(controlPoints[side, j, k].vert.Y);
                            singlePoints.Add(controlPoints[side, j, k].vert.Z);
                        }
                    break;
                case 1:

                    for (int j = 0; j < 4; j++)
                        for (int k = 0; k < 4; k++)
                        {
                            singlePoints.Add(controlPoints[j, side, k].vert.X);
                            singlePoints.Add(controlPoints[j, side, k].vert.Y);
                            singlePoints.Add(controlPoints[j, side, k].vert.Z);
                        }
                    break;

                case 2:

                    for (int j = 0; j < 4; j++)
                        for (int k = 0; k < 4; k++)
                        {
                            singlePoints.Add(controlPoints[j, k, side].vert.X);
                            singlePoints.Add(controlPoints[j, k, side].vert.Y);
                            singlePoints.Add(controlPoints[j, k, side].vert.Z);
                        }
                    break;
            }

            gl.ClearColor(0.0f, 0.0f, 0.0f, 0.0f);
            gl.Enable(OpenGL.GL_DEPTH_TEST);
            gl.Map2(OpenGL.GL_MAP2_VERTEX_3, 0, 1, 3, 4, 0, 1, 12, 4, singlePoints.ToArray());
            gl.Enable(OpenGL.GL_MAP2_VERTEX_3);
            gl.Enable(OpenGL.GL_AUTO_NORMAL);
            gl.MapGrid2(20, 0.0, 1.0, 20, 0.0, 1.0);
            gl.EvalMesh2(OpenGL.GL_FILL, 0, 20, 0, 20);
        }
        private void DisplayInside()
        {
            VertexEx tmp;
            if (ShowControlPoints)
            {
                gl.Disable(OpenGL.GL_LIGHTING);
                gl.Disable(OpenGL.GL_TEXTURE);
                gl.Color(1.0f, 0.0, 0.0);
                foreach (VertexEx p in controlPoints)
                {

                    gl.Begin(OpenGL.GL_LINES);
                    for (int i = 0; i < p.neighboursCoord.Count; i++)
                    {
                        if (p.neighboursCoord[i].Z == -1)
                            continue;
                        gl.Vertex(p.vert.X, p.vert.Y, p.vert.Z);
                        tmp = controlPoints[(int)p.neighboursCoord[i].X, (int)p.neighboursCoord[i].Y, (int)p.neighboursCoord[i].Z];
                        gl.Vertex(tmp.vert.X, tmp.vert.Y, tmp.vert.Z);
                    }
                    gl.End();

                    gl.PointSize(5);
                    gl.Begin(OpenGL.GL_POINTS);
                    gl.Vertex(p.vert.X, p.vert.Y, p.vert.Z);
                    gl.End();
                }


                gl.Enable(OpenGL.GL_TEXTURE);
                gl.Enable(OpenGL.GL_LIGHTING);
            }


            else
            {
                DisplayBezierSurfaces(0, 0);
                DisplayBezierSurfaces(0, 3);
                DisplayBezierSurfaces(1, 0);
                DisplayBezierSurfaces(1, 3);
                DisplayBezierSurfaces(2, 0);
                DisplayBezierSurfaces(2, 3);
            }
            if (ShowSteringCube)
                DisplayBoundingInside();
        }
        private void DisplayBoundingInside()
        {
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_TEXTURE);
            gl.Color(1.0f, 1.0f, 1.0f, 0.5f);

            gl.Begin(OpenGL.GL_LINES);

            for (int i = 0; i < insideCubeIndices.Length; i += 2)
            {
                gl.Vertex(insideCubeVertices[insideCubeIndices[i]].X, insideCubeVertices[insideCubeIndices[i]].Y, insideCubeVertices[insideCubeIndices[i]].Z);
                gl.Vertex(insideCubeVertices[insideCubeIndices[i + 1]].X, insideCubeVertices[insideCubeIndices[i + 1]].Y, insideCubeVertices[insideCubeIndices[i + 1]].Z);
            }

            gl.Vertex(insideCubeVertices[0].X, insideCubeVertices[0].Y, insideCubeVertices[0].Z);
            gl.Vertex(controlPoints[0, 0, 0].vert.X, controlPoints[0, 0, 0].vert.Y, controlPoints[0, 0, 0].vert.Z);

            gl.Vertex(insideCubeVertices[1].X, insideCubeVertices[1].Y, insideCubeVertices[1].Z);
            gl.Vertex(controlPoints[3, 0, 0].vert.X, controlPoints[3, 0, 0].vert.Y, controlPoints[3, 0, 0].vert.Z);

            gl.Vertex(insideCubeVertices[2].X, insideCubeVertices[2].Y, insideCubeVertices[2].Z);
            gl.Vertex(controlPoints[0, 3, 0].vert.X, controlPoints[0, 3, 0].vert.Y, controlPoints[0, 3, 0].vert.Z);

            gl.Vertex(insideCubeVertices[3].X, insideCubeVertices[3].Y, insideCubeVertices[3].Z);
            gl.Vertex(controlPoints[0, 0, 3].vert.X, controlPoints[0, 0, 3].vert.Y, controlPoints[0, 0, 3].vert.Z);

            gl.Vertex(insideCubeVertices[4].X, insideCubeVertices[4].Y, insideCubeVertices[4].Z);
            gl.Vertex(controlPoints[3, 3, 0].vert.X, controlPoints[3, 3, 0].vert.Y, controlPoints[3, 3, 0].vert.Z);

            gl.Vertex(insideCubeVertices[5].X, insideCubeVertices[5].Y, insideCubeVertices[5].Z);
            gl.Vertex(controlPoints[3, 0, 3].vert.X, controlPoints[3, 0, 3].vert.Y, controlPoints[3, 0, 3].vert.Z);

            gl.Vertex(insideCubeVertices[6].X, insideCubeVertices[6].Y, insideCubeVertices[6].Z);
            gl.Vertex(controlPoints[0, 3, 3].vert.X, controlPoints[0, 3, 3].vert.Y, controlPoints[0, 3, 3].vert.Z);

            gl.Vertex(insideCubeVertices[7].X, insideCubeVertices[7].Y, insideCubeVertices[7].Z);
            gl.Vertex(controlPoints[3, 3, 3].vert.X, controlPoints[3, 3, 3].vert.Y, controlPoints[3, 3, 3].vert.Z);

            gl.End();
            gl.Enable(OpenGL.GL_TEXTURE);
            gl.Enable(OpenGL.GL_LIGHTING);
        }
        private void DisplayBoundingCube(int mode)
        {
            if (ShowBoundingLines)
            {
                gl.Disable(OpenGL.GL_LIGHTING);
                gl.Disable(OpenGL.GL_TEXTURE);
                gl.Color(0.0, 0.0, 1.0f);
                gl.Begin(OpenGL.GL_LINES);
                for (int i = 0; i < 6; i++)
                    for (int j = 0; j < 4; j++)
                    {
                        gl.Vertex(vertices2[i * 12 + (j * 3)], vertices2[i * 12 + (j * 3) + 1], vertices2[i * 12 + (j * 3) + 2]);
                        if (j != 3)
                            gl.Vertex(vertices2[i * 12 + (j * 3) + 3], vertices2[i * 12 + (j * 3) + 4], vertices2[i * 12 + (j * 3) + 5]);
                        else
                            gl.Vertex(vertices2[i * 12], vertices2[i * 12 + 1], vertices2[i * 12 + 2]);
                    }
                gl.End();
                gl.Enable(OpenGL.GL_TEXTURE);
                gl.Enable(OpenGL.GL_LIGHTING);
            }
            else
            {
                gl.Enable(OpenGL.GL_BLEND);
                gl.BlendFunc(OpenGL.GL_ONE, OpenGL.GL_ONE_MINUS_SRC_COLOR);
                gl.EnableClientState(OpenGL.GL_NORMAL_ARRAY);
                gl.EnableClientState(OpenGL.GL_VERTEX_ARRAY);
                gl.NormalPointer(OpenGL.GL_FLOAT, 0, normals2);
                gl.VertexPointer(3, 0, vertices2);
                gl.DrawElements(OpenGL.GL_TRIANGLES, 36, indices);
                gl.DisableClientState(OpenGL.GL_VERTEX_ARRAY);
                gl.DisableClientState(OpenGL.GL_NORMAL_ARRAY);
                gl.Disable(OpenGL.GL_BLEND);
            }
        }
        private void InitializeVariables()
        {
            _scale = 0.3f;
            _traY = _traX = -0.5f;
            _mouseX = _mouseY = 0;
            _rotateZ = _rotateVer = 0.0f;
            _rotateHor = 90.0f;
            _lengthSteringCube = 1;
            _maxStartVel = 0.3;
            _m = 0.05;
            _k = 0.1;
            _c1 = 0.5;
            _c2 = 0.1;
            _isMouseDown = false;
            _showControlPoints = _showSteringCube = _showBoundingCubeLines = true;
        }

        private Vertex VertexBetweenPoints(int i, int j, int k, int nIndex)
        {
            Vertex a = controlPoints[i, j, k].vert;
            Vertex index = controlPoints[i, j, k].neighboursCoord[nIndex];
            Vertex b = index.Z == -1 ? insideCubeVertices[(int)index.X] : controlPoints[(int)index.X, (int)index.Y, (int)index.Z].vert;
            return new Vertex(Math.Abs(b.X - a.X), Math.Abs(b.Y - a.Y), Math.Abs(b.Z - a.Z));
        }
        private List<Vertex> CalculateStartDistancesVertices(int i, int j, int k)
        {
            List<Vertex> distances = new List<Vertex>();

            for (int a = 0; a < controlPoints[i, j, k].neighboursCoord.Count; a++)
                distances.Add(VertexBetweenPoints(i, j, k, a));

            return distances;
        }
        private List<double> CalculateStartDistances(int i, int j, int k)
        {
            List<double> distances = new List<double>();

            for (int a = 0; a < controlPoints[i, j, k].neighboursCoord.Count; a++)
                distances.Add(Length(VertexBetweenPoints(i, j, k, a)));

            return distances;
        }
        private double Length(Vertex a)
        {
            return Math.Sqrt(a.X * a.X + a.Y * a.Y + a.Z * a.Z);
        }

        private void simulationTick(object sender, EventArgs e)
        {
            List<Vertex> actualNeighbours;
            Vertex index;
            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++)
                    for (int k = 0; k < 4; k++)
                    {
                        actualNeighbours = new List<Vertex>();
                        for (int a = 0; a < controlPoints[i, j, k].neighboursCoord.Count; a++)
                        {
                            if (controlPoints[i, j, k].neighboursCoord[a].Z == -1)
                                actualNeighbours.Add(insideCubeVertices[(int)controlPoints[i, j, k].neighboursCoord[a].X]);
                            else
                            {
                                index = controlPoints[i, j, k].neighboursCoord[a];
                                actualNeighbours.Add(controlPoints[(int)index.X, (int)index.Y, (int)index.Z].vert);
                            }
                        }
                        pointSimulations[i, j, k].CalculateXXtXtt(actualNeighbours);
                    }

            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++)
                    for (int k = 0; k < 4; k++)
                    {
                        controlPoints[i, j, k].vert = pointSimulations[i, j, k].newPosition;
                        pointSimulations[i, j, k].Collisions(boundingCubeFront, boundingCubeBack, (float)_u, IdealCollisions);
                    }

            //for (int i = 0; i < 4; i++)
            //    for (int j = 0; j < 4; j++)
            //        for (int k = 0; k < 4; k++)
        }

        private void TimerRestart()
        {
            _simulationTimer.Stop();
            BuildInsideCube();
            _simulationTimer.Start();
        }


    }
}
