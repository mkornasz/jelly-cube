﻿using SharpGL.SceneGraph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSW2_Jelly
{
    class VertexEx
    {

        public Vertex vert;
        public double startPosition;
        public List<Vertex> neighboursCoord;
        public List<Vertex> neighboursCoordDistances;
        public List<double> neighboursDistances;

        public VertexEx(Vertex v, List<Vertex> l, double vel = 0, double pos = 0)
        {
            vert = v;
            // nextVert = v;
            startPosition = pos;
            neighboursCoord = new List<Vertex>(l);
            neighboursCoordDistances = new List<Vertex>();
            neighboursDistances = new List<double>();
        }

    }
}
