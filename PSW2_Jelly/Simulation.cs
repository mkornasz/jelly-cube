﻿using System;
using System.Collections.Generic;
using SharpGL.SceneGraph;

namespace PSW2_Jelly
{
    class Simulation
    {
        private int iteration = 0;
        private float _mass;
        private double _c1, _c2, _k, _delta;
        private List<Vertex> _startDistances;
        private List<double> _startDistancesDouble;
        private Vertex newPos, lastPos, nextPos, nextLength, prevLength;
        private Vertex velocity;
        private Vertex startPos;
        #region Propierties

        public Vertex newPosition { get { return newPos; } set { newPos = value; } }
        public Vertex lastPosition { get { return lastPos; } set { lastPos = value; } }
        public Vertex nextPosition { get { return nextPos; } set { nextPos = value; } }
        #endregion

        public Simulation(double mass, double c1, double c2, double k, Vertex actual, List<double> startDistancesDouble, List<Vertex> startDistances, Vertex start, Vertex startVel)
        {
            _mass = (float)mass;
            _c1 = c1;
            _c2 = c2;
            _k = k;
            _delta = 0.01;
            newPos = actual;
            lastPos = new Vertex(0, 0, 0);
            nextPos = new Vertex(0, 0, 0);
            _startDistancesDouble = new List<double>(startDistancesDouble);
            _startDistances = new List<Vertex>(startDistances);
            velocity = startVel;
            startPos = start;
            prevLength = new Vertex(0, 0, 0);
        }
        
        
        //public void CalculateXXtXtt(double time, List<Vertex> neighbours)
        //{

        //    if (iteration == 0)
        //    {
        //        nextPos = GetSecondPosition(newPos, velocity);
        //        velocity = GetCurrentVelocity(nextPos, newPos);
        //    }

        //    else
        //    {
        //        Vertex ff = new Vertex(0, 0, 0);
        //        Vertex P1 = newPos;
        //        Vertex V1 = velocity;
        //        for (int i = 0; i < neighbours.Count; i++)
        //        {
        //            Vertex P2 = neighbours[i];
        //            _actualLengths[i] = VertexBetweenPoints(newPos, neighbours[i]);
        //            //var bb = _actualLengths[i] + _startDistances[i];
        //            //var lp = Divide(_actualLengths[i] - _prevLengths[i], (2 * (float)_delta));
        //            //ff += (Multiple(lp, (-1.0f) * (float)_k) - Multiple(bb, (float)_c1));
        //            //_prevLengths[i] = _actualLengths[i];
        //            ff += GetCurrentForce(P1, P2, _startDistances[i], V1);
        //        }

        //        velocity = GetNextVelocity(ff, velocity);
        //        nextPos = GetSecondPosition(newPos, velocity);
        //    }
        //    //   lastPos = newPos;
        //    //  newPos = nextPos;
        //    //var a = Divide(ff, _mass);
        //    //var n = Multiple(a, (float)_delta * (float)_delta) + Multiple(newPos, 2) - lastPos;
        //    //lastPos = newPos;
        //    //newPos = n;
        //    iteration++;
        //}

        public void CalculateXXtXtt( List<Vertex> neighbours)
        {
            if (iteration == 0)
            {
                nextPos= newPos + Multiple(velocity , _delta);
                lastPos = newPos;
                newPos = nextPos;
            }

            else
            {
                nextLength = new Vertex(0, 0, 0);
                Vertex np = new Vertex(0, 0, 0);
                Vertex ff = new Vertex(0, 0, 0);
                for (int i = 0; i < neighbours.Count; i++)
                {
                    var bb = neighbours[i] - newPos;
                    Vertex nl;
                    double len = Length(bb);
                    double Rlength = _startDistancesDouble[i];
                    double dX = len - Rlength;
                    double FstructMag = _startDistancesDouble[i] == 0 ? -1 * _c2 * dX : -1 * _c1 * dX;
                    nl = Normalize(bb);
                    nl = Multiple(nl, FstructMag);
                    nextLength += nl;

                }

                for (int i = 0; i < neighbours.Count; i++)
                {
                    var lp = Divide(nextLength - prevLength, (2 * (float)_delta));
                    ff += (Multiple(lp, (-1.0f) * (float)_k) - Multiple(nextLength, (float)_c1));
                    prevLength = nextLength;
                }


                var a = Divide(ff, _mass);
                var n = Multiple(a, (float)_delta * (float)_delta) + Multiple(newPos, 2) - lastPos;


                lastPos = newPos;
                newPos = n;
            }
            iteration++;
        }

        public void Collisions(Vertex front, Vertex back, float u, bool isIdeal = false)
        {
            int recursion = 0;
            Vertex n;
            float change;
            while (recursion < 3)
            {
                if (newPos.X <= front.X || newPos.X >= back.X)
                {
                    if (isIdeal)
                    {
                        change = lastPos.X + u * newPos.X - u * lastPos.X;
                        newPos.X = lastPos.X;
                        lastPos.X = change;

                    }
                    else
                    {
                        n = lastPos + Multiple(newPos, (float)u) - Multiple(lastPos, (float)u);
                        newPos = lastPos;
                        lastPos = n;
                    }
                }

                if (newPos.Y <= front.Y || newPos.Y >= back.Y)
                {
                    if (isIdeal)
                    {
                        change = lastPos.Y + u * newPos.Y - u * lastPos.Y;
                        newPos.Y = lastPos.Y;
                        lastPos.Y = change;
                    }
                    else
                    {
                        n = lastPos + Multiple(newPos, (float)u) - Multiple(lastPos, (float)u);
                        newPos = lastPos;
                        lastPos = n;
                    }
                }
                if (newPos.Z <= front.Z || newPos.Z >= back.Z)
                {
                    if (isIdeal)
                    {
                        change = lastPos.Z + u * newPos.Z - u * lastPos.Z;
                        newPos.Z = lastPos.Z;
                        lastPos.Z = change;
                    }
                    else
                    {
                        n = lastPos + Multiple(newPos, (float)u) - Multiple(lastPos, (float)u);
                        newPos = lastPos;
                        lastPos = n;
                    }
                }
                recursion++;
            }
        }

        #region Vectors Operation
        private Vertex Divide(Vertex a, double b)
        {
            return new Vertex(a.X / (float)b, a.Y / (float)b, a.Z / (float)b);
        }
        private Vertex Multiple(Vertex a, double b)
        {

            return new Vertex(a.X * (float)b, a.Y * (float)b, a.Z * (float)b);
        }
        private Vertex VertexBetweenPoints(Vertex actual, Vertex another)
        {
            return new Vertex(actual.X - another.X, actual.Y - another.Y, actual.Z - another.Z);
        }
        private double Length(Vertex a)
        {
            return Math.Sqrt(a.X * a.X + a.Y * a.Y + a.Z * a.Z);
        }
        private Vertex Normalize(Vertex a)
        {
            float len = (float)Length(a);
            if (len == 0)
                return new Vertex(0, 0, 0);
            a.X /= len;
            a.Y /= len;
            a.Z /= len;
            return a;
        }
        public double Distance(Vertex a, Vertex b)
        {
            return Math.Sqrt((a.X - b.X) * (a.X - b.X) + (a.Y - b.Y) * (a.Y - b.Y) + (a.Z - b.Z) * (a.Z - b.Z));
        }
        #endregion
    }


}
